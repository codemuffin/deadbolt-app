## Programs

### [UndertaleModTool (UMT)](https://github.com/krzys-h/UndertaleModTool)

The tool that made modding possible. If you want to mod Deadbolt, this lets you do it.

### [GameMaker Studio 1.4.1749](https://archive.org/download/GMStudio1.4.x/GMStudio-Installer-1.4.1749.exe)

Download Deadbolt's game engine and correct build version. Links is via [archive.org](https://archive.org/details/GMStudio1.4.x)


## Sprite Extraction

### [YAL Sprite viewer](https://yal.cc/r/17/yytextureview/)

Save sprites as single images or strips.


## Misc

### [GML Documentation](https://web.archive.org/web/20191010010940/https://docs.yoyogames.com/)

An archived version of the old GML documentation site. This is where you'll learn how to program in GML, which you'll need to use to mod Deadbolt. If you understand basic programming concepts, most of it will make sense.

### [UndertaleModTool-ExportToProjectScript](https://github.com/cubeww/UndertaleModTool-ExportToProjectScript)

Use this with UMT to export Deadbolt to an project, which you can open with GMS 1.4. Only works with UMT [v0.3.5.7](https://github.com/krzys-h/UndertaleModTool/releases/tag/0.3.5.7) or below. Use [this PR](https://github.com/ithinkandicode/UndertaleModTool-ExportToProjectScript/tree/fix/12-colkind-BBoxMode), which fixes a breaking issue.

### [Deadbolt-GMS-Issues](https://github.com/ithinkandicode/Deadbolt-GMS-1.4-Misc/issues)

A github repo with some info on running Deadbolt in GMS. The main attraction is the Issues, which will help you fix most of the problems you'll face when trying to run DB in GMS1.4 _(Please don't add issues there, they won't be seen; use the modding Discord instead)._
